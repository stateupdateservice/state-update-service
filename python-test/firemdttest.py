import sys
sys.path.append('../python-api')
import state_update_service_api
s = state_update_service_api.StateService(None, "https://www.firemdt.com:9299/state")
sys.path.append('/home/garret/apps')
from firemdt_pb2 import *

orgtowatch = "org-delaware-mcfc"

s.initKey( orgtowatch, Organization())
org = s.getKeys([ orgtowatch ]) [0]

def checkincidents( org, seen ):
	incidentstofetch = []
	for inc in org.currentIncidents:
		if not inc in seen:
			incidentstofetch.append("incident-"+inc)
	
	for incKey in incidentstofetch:
		s.initKey( incKey, Incident())

	incObjs = s.getKeys( incidentstofetch )
	for incObj in incObjs:
		print "New incident:"
		print incObj
		seen.append(incObj.id)

seenIncidents = []
checkincidents( org, seenIncidents )

s.watchKey( orgtowatch )
while True:
	print "Watching organization..."
	updates = s.doWatch()
	if updates.has_key(orgtowatch):
		org = updates[orgtowatch]
		checkincidents( org, seenIncidents )
		
