import zmq
import threading
import time
from test_proto_pb2 import *

import sys

sys.path.append('../python-api')
import state_update_service_api

class Watcher(threading.Thread):
	def __init__(self, service):
		threading.Thread.__init__(self)
		self.service = service
		self.daemon = True

	def run(self):
		while True:
			time.sleep(1)
			print "Watching",s.watchKeys.keys()
			res = s.doWatch()
			for k in res.keys():
				print "Update Key:",k,"Value:",res[k]
			

c = zmq.Context(1)

s = state_update_service_api.StateService(c, sys.argv[1] )

ob = Test()
ob.a = "hi"
ob.b.append( 123 )
ob.b.append( 432 )
ob.b.append( 5 )

# Test basic update/get functionality
s.updateKey( "k", ob )
s.flushUpdates()

s.initKey( "k", Test() )
assert s.getKeys( [ "k" ] )[ 0 ] == ob

# Test partial update repeated functionality
s.partialUpdateKey( "k", Test( b = [ 323 ] ) )
s.partialUpdateKey( "k", Test( b = [ 999 ] ) )
assert not s.flushUpdates()
ob.b.append( 323 )
ob.b.append( 999 )

assert s.getKeys( [ "k" ] )[ 0 ] == ob

# Test partial update non-repeated functionality
s.partialUpdateKey( "k", Test( a = "zzz" ) )
assert not s.flushUpdates()
s.partialUpdateKey( "k", Test( a = "wow" ) )
assert not s.flushUpdates()

# Make sure value is correct
ob.a = "wow"
assert s.getKeys( [ "k" ] )[ 0 ] == ob

w = Watcher( s )
w.start()
time.sleep(1.5)

s.watchKey( "k" )
s.partialUpdateKey( "k", Test( a = "watching" ) )
assert not s.flushUpdates()

raw_input("")
