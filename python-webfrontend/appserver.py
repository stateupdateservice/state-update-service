#!/usr/bin/python
"""A web.py application powered by gevent"""

from gevent import monkey; monkey.patch_all()
from gevent.pywsgi import WSGIServer

from gevent_zeromq import zmq

import gevent
import time
import web
import os
import sys

HOME = os.environ['HOME']

urls = ('/(|index\.html)', 'static',
        '/state', 'state_update_server')

listenport = 1235

context = zmq.Context()

class static:
    def GET(self, page):
	if page == '':
		page = 'index.html'
	# Because every page is enumerated in the urls, we dont have to worry about checking the path
	f = open('static/%s' % (page))
	contents = f.read()
	f.close()
	if page.endswith('.js'):
		web.header('Content-Type', 'application/x-javascript')
	elif page.endswith('.swf'):
		web.header('Content-Type', 'application/x-shockwave-flash')
	elif page.endswith('.svg'):
		web.header('Content-Type', 'image/svg+xml')
	elif page.endswith('.json'):
		web.header('Content-Type', 'application/json')
	elif page.endswith('.png'):
		web.header('Content-Type', 'image/png')
	return contents

class state_update_server:
	def POST(self):
		inpacket = web.data()
		socket = context.socket(zmq.REQ)
		socket.connect ("tcp://localhost:1234")
		socket.send( inpacket )
		returnpacket = socket.recv()
		web.header('Content-Type', 'application/octet-stream')
		socket.close()
		return returnpacket
		

if __name__ == "__main__":
    application = web.application(urls, globals()).wsgifunc()
    print 'Serving on %d...' % (listenport)
    WSGIServer(('', listenport), application).serve_forever()

