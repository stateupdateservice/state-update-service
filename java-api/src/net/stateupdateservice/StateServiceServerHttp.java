package net.stateupdateservice;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import com.google.protobuf.InvalidProtocolBufferException;

import net.stateupdateservice.StateServiceServer;
import net.stateupdateservice.ServiceMessage.ServicePacket;

public class StateServiceServerHttp implements StateServiceServer {
	private String serverurl;

	public StateServiceServerHttp( String server ) {
		serverurl = server;
	}

	public ServicePacket sendRequest( ServicePacket packet ) throws IOException {
		URL url = new URL( serverurl );
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);

		OutputStream wr = conn.getOutputStream();
		wr.write(packet.toByteArray());
		wr.flush();
	 
		// Get the response
		InputStream rd = conn.getInputStream();
		byte[] buf = new byte[1024*512];
		ByteArrayOutputStream readStream = new ByteArrayOutputStream();
		int bufferLength = 0;
		while ((bufferLength = rd.read(buf)) != -1) {
		    readStream.write(buf, 0, bufferLength);
		}
		wr.close();
		rd.close();
		
		// Build return packet
		ServicePacket.Builder returnPacket = ServicePacket.newBuilder();
		byte[] vals = readStream.toByteArray();
		returnPacket.mergeFrom(vals);
		return returnPacket.build();
	}
}
