package net.stateupdateservice;
import java.io.IOException;

import net.stateupdateservice.ServiceMessage.ServicePacket;

public interface StateServiceServer {
	ServicePacket sendRequest( ServicePacket packet ) throws IOException;
}
