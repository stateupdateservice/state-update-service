package net.stateupdateservice;
import java.io.IOException;
import java.io.Serializable;

import com.google.protobuf.*;

public class CacheEntry implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8224425716234396415L;
	
	public String key;
	private transient Message.Builder value;
	public String session;
	public long revision;
	private byte[] packetValue;

	public CacheEntry( String key, Message.Builder value, String session, Integer revision ) {
		this.key = key;
		this.value = value.clone();
		this.packetValue = this.value.buildPartial().toByteArray();
		this.session = session;
		this.revision = revision;
	}
	public void initValue ( Message.Builder msg ) {
		// If we have been recently loaded from disk cache, we may need to merge the message bytes in
		if( this.value == null ){
			this.value = msg.clone();
			try {
				this.value.mergeFrom(this.packetValue);
			} catch (InvalidProtocolBufferException e) {
				// Ignore
			}
		}
	}
	
	public Message.Builder getValue() {
		return this.value.clone();
	}
	
	public void setValue( Message.Builder msg ) {
		this.value = msg.clone();
		this.packetValue = this.value.build().toByteArray();
	}
	
	public void mergeValueBytes( ByteString msg ) throws InvalidProtocolBufferException {
		this.value.mergeFrom(msg);
		this.packetValue = this.value.buildPartial().toByteArray();		
	}
	
	public void setValueBytes( ByteString msg ) throws InvalidProtocolBufferException {
		this.value.clear().mergeFrom(msg);
		this.packetValue = this.value.buildPartial().toByteArray();		
	}
}
