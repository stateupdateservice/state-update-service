
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.protobuf.Message;

import testproto.TestProto.Test;
import net.stateupdateservice.StateService;
import net.stateupdateservice.StateServiceServerHttp;


public class SUSTest {
	public static class Watcher extends Thread {
		private StateService service;

		public Watcher (StateService serviceHandle) {
			this.setDaemon(true);
			service = serviceHandle;
		}

		@Override
		public void run() {
			try {
				while(true) {
					System.out.println("Watching " + service.watchKeys.keySet().toString() );
							
					Thread.sleep(1000);
					Map<String,Message> results = service.doWatch();
					for( Entry<String,Message> entry : results.entrySet() ) {
						System.out.println("Update key: " + entry.getKey() + " Value: "+ entry.getValue().toString() );			
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public static void main( String[] args ) {
		
		StateServiceServerHttp s = new StateServiceServerHttp( "http://localhost:1235/state" );
		StateService serv = new StateService(s, null);

		try {
			Test.Builder t = Test.newBuilder().setA( "hi" ).addB( 123 ).addB( 432 ).addB( 5 );

			// Test basic update/get functionality
			serv.updateKey( "k", t );
			if( !serv.flushUpdates().isEmpty() ) {
				System.out.println("Service failed to flush initial update.");
				return;
			}
			serv.initKey( "k", Test.newBuilder() );
			List<Message> msgs = serv.getKeys( new String[]{ "k" }, true );
			
			if( !t.build().equals(msgs.get(0)) ) {
				System.out.println("Service didn't properly store/return initial key set!");
				return;
			}

			// Test partial update repeated functionality
			serv.partialUpdateKey( "k", Test.newBuilder().addB( 323 ) );
			serv.partialUpdateKey( "k", Test.newBuilder().addB( 999 ) );
			if( !serv.flushUpdates().isEmpty() ) {
				System.out.println("Service failed to flush partial update.");
				return;
			}
			t.addB( 323 ).addB( 999 );
			
			if( !t.build().equals( serv.getKeys( new String[]{ "k" }, true ).get(0) ) ) {
				System.out.println("Service didn't properly store/return partial update");
				return;
			}

			// Test partial update non-repeated functionality
			serv.partialUpdateKey( "k", Test.newBuilder().setA( "zzz" ) );
			if( !serv.flushUpdates().isEmpty() ) {
				System.out.println("Service failed to flush partial update 2.");
				return;
			}
			serv.partialUpdateKey( "k", Test.newBuilder().setA( "wow" ) );
			if( !serv.flushUpdates().isEmpty() ) {
				System.out.println("Service failed to flush partial update 3.");
				return;
			}

			// Make sure value is correct
			t.setA( "wow" );
			if( !t.build().equals(serv.getKeys( new String[]{ "k" }, true ).get(0)) ) {
				System.out.println("Service didn't properly store/return non-repeated partial update");
				return;
			}
			
			Watcher w = new Watcher( serv );
			
			w.start();
			// Start thread, then wait a little before watching
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			// Watch key, k
			try {
				serv.watchKey("k", true);
			} catch (IOException e) {
				System.out.println("IOException while talking to server: " + e.toString());
			}

			// Trigger key update
			serv.partialUpdateKey( "k", Test.newBuilder().setA( "watching" ) );
			if( !serv.flushUpdates().isEmpty() ) {
				System.out.println("Service failed to flush partial update while watching.");
				return;
			}
			
		} catch( IOException e ) {
			System.out.println("IOException while talking to server: " + e.toString());
			return;
		}
		
		System.out.println("Waiting a little while before terminating.");
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Tests passed!");
		return;
	}
}
